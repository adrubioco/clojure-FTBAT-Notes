(ns core.main
  (:require core.chapter4 :as ch4))

(defn -main
  [& args]
  (let [filename (first args)]
    (ch4/convert :glitter-index "3")
    (ch4/parse (slurp filename))
    (first (ch4/mapify (ch4/parse (slurp filename))))
    (ch4/glitter-filter 3 (ch4/mapify (ch4/parse (slurp filename))))))
