(ns core.chapter4)

(def filename "resources/vampire-data.csv")

(def vamp-keys [:name :glitter-index])

(defn str->int
  [str]
  (Integer. str))

(def conversions {:name identity
                  :glitter-index str->int})

(defn convert
  [vamp-key value]
  ((get conversions vamp-key) value))

(defn parse
  "Convert a CSV into rows of columns"
  [string]
  (map #(clojure.string/split % #",")
       (clojure.string/split string #"\n")))

(defn mapify
  "Return a seq of maps like {:name \"Edward Cullen\" :glitter-index 10}"
  [rows]
  (map (fn [unmapped-row]
         (reduce (fn [row-map [vamp-key value]]
                   (assoc row-map vamp-key (convert vamp-key value)))
                 {}
                 (map vector vamp-keys unmapped-row)))
       rows))

(defn glitter-filter
  [minimum-glitter records]
  (filter #(>= (:glitter-index %) minimum-glitter) records))

;; Pre exercises
(def suspects (glitter-filter 3 (mapify (parse (slurp filename)))))

;; Exercise 1
(map :name suspects)
;;; Alternative solutions
(map #(:name %) suspects)
(map #(get % :name) suspects)
(map (fn [map-entry] (get map-entry :name))
     suspects)

;; Exercise 2
(defn append
  [suspects suspect]
  (conj suspects suspect))

(assoc {} :a "a")
(append suspects {:name "Dracacula" :glitter-index 10})

;; Exercise 3
(def dracacula {:name "Dracacula" :glitter-index 10})

(def validators {:name #(get % :name)
                 :glitter-index #(get % :glitter-index)})

(defn validate
  "Checks that a record contains the expected keywords"
  [validators records]
  (map #(records %) validators))

(defn append-validating
  [suspects suspect]
  (conj (suspects (validate {:name :name :glitter-index :glitter-index}
                            suspect))))

(validate validators dracacula)

(append-validating suspects {:name "Dracacula" :glitter-index 10})
